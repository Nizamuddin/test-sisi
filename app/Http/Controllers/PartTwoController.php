<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class PartTwoController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function numberThreePage() {
        return view('part-two.number-three');
    }

    public function numberThreeProcess(Request $request) {
        try {
            $number = $request->number;
            $strOutput = '';
            for ($i=0; $i <= $number; $i++) { 
                $strOutput .= $i ;
                $concat = '';
                if ($i % 3 == 0) {
                    $concat .= 'foo';
                }
                if ($i % 5 == 0) {
                    $concat .= 'bar';
                }
                $strOutput .= ' '.$concat. '<br>';
            }
            echo $strOutput;
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function numberFourPage() {
        return view('part-two.number-four');
    }

    public function numberFourProcess(Request $request) {
        try {
            $text = $request->text;
            $explodeText = explode(' ', $text);
            $strResult = '';
            foreach ($explodeText as $key => $word) {
                $strResult .= strrev($word).' ';
            }
            echo $strResult;
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function numberFivePage(){
        return view('part-two.number-five');
    }

    function numberFiveProcess(Request $request){
        $validated = $request->validate([
            'number' => 'required|regex:/^\d+(?:,\d+)*$/',
        ]);
        $numbers = explode(',', $request->number);
        $resTemp = [];
        foreach ($numbers as $key => $number) {
            if(!isset($resTemp[$number])){
                $resTemp[$number] = 1;
            }else{
                $resTemp[$number]++;
            }
        }
        $result = '';
        foreach ($resTemp as $key => $value) {
            if($value == 1){
                $result .= $key.',' ;
            }
        }
        dd($result);
    }
}
