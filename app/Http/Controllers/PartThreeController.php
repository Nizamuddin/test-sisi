<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Illuminate\Routing\Controller as BaseController;
use Yajra\DataTables\DataTables as DataTablesDataTables;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;

class PartThreeController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function getProduct(Request $request)  {
        if ($request->ajax()) {
            $data = Product::select('*')->orderBy('id', 'desc');
            return DataTablesDataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
       
                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
   
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
      
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
          
        return view('users');

    }
    public function saveProduct(Request $request) {
        Product::updateOrCreate([
            'id' => $request->product_id
        ],
        [
            'name' => $request->name, 
            'code' => $request->code,
            'price' => $request->price,
        ]);        

        return response()->json(['success'=>'Product saved successfully.']);
        // return view('part-two.number-three');
    }

    public function getDetailProduct(Request $request){
        $product = Product::find($request->id);
        return response()->json($product);
    }

    public function deleteProduct(Request $request)
    {
        Product::find($request->id)->delete();
      
        return response()->json(['success'=>'Product deleted successfully.']);
    }

    public function addTransactionPage(){
        $products = Product::get();
        return view('part-three.transaction-form', ['products' => $products]);
    }
}
