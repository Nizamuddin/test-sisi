<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number Three</title>
</head>
<body>
    <form action="{{ route('nomor-3-process') }}" method="POST">
    @csrf
        <label for="">Jumlah angka</label>
        <input type="number" name="number" id="number">
        <button>SUBMIT</button>
    </form>
    <a href="{{ route('home') }}"> HOME PAGE </a>
</body>
</html>