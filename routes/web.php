<?php

use App\Http\Controllers\PartThreeController;
use App\Http\Controllers\PartTwoController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::group(['prefix' => 'part-2'], function (): void {
    Route::get('/nomor-3', [PartTwoController::class, 'numberThreePage'])->name('nomor-3-page');
    Route::post('/nomor-3', [PartTwoController::class, 'numberThreeProcess'])->name('nomor-3-process');

    Route::get('/nomor-4', [PartTwoController::class, 'numberFourPage'])->name('nomor-4-page');
    Route::post('/nomor-4', [PartTwoController::class, 'numberFourProcess'])->name('nomor-4-process');

    Route::get('/nomor-5', [PartTwoController::class, 'numberFivePage'])->name('nomor-5-page');
    Route::post('/nomor-5', [PartTwoController::class, 'numberFiveProcess'])->name('nomor-5-process');
});
Route::group(['prefix' => 'part-three'], function (): void {
    Route::get('/', function () {
        return view('part-three.dashboard');
    })->name('index-dashboard');
    Route::get('/get-product', [PartThreeController::class, 'getProduct'])->name('get-product');
    Route::get('/get-detail-product', [PartThreeController::class, 'getDetailProduct'])->name('get-detail-product');
    Route::delete('/delete', [PartThreeController::class, 'deleteProduct'])->name('delete-product');
    Route::post('/save-product', [PartThreeController::class, 'saveProduct'])->name('save-product');

    Route::get('/transaction', function () {
        return view('part-three.transaction');
    })->name('index-transaction');

});

Route::resource('transaction', TransactionController::class);
Route::get('/transaction/data/get', [TransactionController::class, 'getData'])->name('transaction.getData');
Route::put('/transaction/editproduct/{id}', [TransactionController::class, 'editProduct'])->name('transaction.editProduct');
Route::delete('/transaction/deleteproduct/{id}', [TransactionController::class, 'deleteProduct'])->name('transaction.deleteProduct');